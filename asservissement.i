// Script d'asservissement pour garder la PSF en place sur un point donné de l'image.
// By Clément Schotte
//@TODO : Ne garder que la ROI.
//@TODO : Dark inutile si on prend les nMost ?
//@TODO : Enregistrer target dans un fichier pour éviter de le refaire.
//@TODO : Mettre une startingPosition. L'enregistrer dans le fichier d'étalonnage.
//@TODO : Voir quelle fréquence on peut atteindre.
//@TODO : Tester avec un DM qui perturbe. < 2 semaines

scriptFolder = "/home/xao/"
minPittValue = [-4000, -1000]
maxPittValue = [-2000, -0000]
nMost = 50 // Looking for the nMost brightest pixels
avgPittValue = (maxPittValue + minPittValue) / 2.

/************* Functions definitions *************/

/*
DOCUMENT cog(image, nMost)
Computes the image's center of gravity using only the nMost brightest pixels.
If more than nMost pixels have the same value, they are all taken into account.
image : matrix
nMost : int
returns a 2 by 1 float array containing the center of gravity's position in the matrix.
*/
func cog(image, nMost) {
    w = 0
    wSum = 0
    wxSum = 0
    wySum = 0
    image = float(image)

    // Position of nMost'th higher value when image is sorted.
    thresholdPosition = numberof(image) - nMost + 1
    // Retrieve the threshold value.
    threshold = image( sort(image(*))(thresholdPosition) )        // sort retourne des indices

    for (x = 1 ; x <= dimsof(image)(2) ; x++) {
        for (y = 1 ; y <= dimsof(image)(3) ; y++) {
            w = image(x, y)
            if(w >= threshold){
                wSum += w
                wxSum += w * x
                wySum += w * y
            }
        }
    }

    if(wSum > 0){
        return [wxSum/wSum , wySum/wSum]
    } else {
        center = dimsof(image) / 2
        return float(center(2:end))
    }
}

/************* Début du script *************/
// Adding "/" at the end of scriptFolder directory.
scriptFolder = scriptFolder + ( strglob("*/", scriptFolder) ? "" : "/" )
// Checking the existence of files
files = lsdir(scriptFolder)
dark = []
saveObj = []
if (structof(files) == long) {
    print,"Directory " + scriptFolder + " does not exist."
} else {
    for (i=1 ; i<=numberof(files) ; ++i){
        if ( strglob("dark.fits", files(i))) {
            dark = fits_read(scriptFolder + "dark.fits")
            break
        }
    }
    for (i=1 ; i<=numberof(files) ; ++i){
        if ( strglob("asservissementValues.save", files(i))) {
            saveObj = openb(scriptFolder + "asservissementValues.save")
            restore, saveObj
            break
        }
    }
}
print,"File dark.fits " + (dark==[] ? "not " : "") + "found."
print,"File asservissementValues.save " + (saveObj==[] ? "not " : "") + "found."
if(saveObj==[]) saveObj = createb(scriptFolder + "asservissementValues.save")

winkill, 0
winkill, 1

pitt = tao_attach_remote_mirror("pitt")
zyla = tao_attach_remote_camera("zyla")
tao_ncpa_set, cam_name="zyla"

tao_start, zyla
tao_visu, zyla, debugmode=1
animate, 1

window, 1
pli, tao_capture(zyla)
write, "Cliquez sur l'emplacement où centrer la PSF.\n"
target = mouse()
target = target(1:2)
print,"Selected target : ", target
winkill, 1
window, 0

if( scale==[] || angle==[] ){
    // Mesuring angle and scale along X axis of tip tilt
    tao_send_commands, pitt, minPittValue
    pause, 200
    tao_send_commands, pitt, [minPittValue(1), avgPittValue(2)]
    pause,1000
    minPsfPos = cog(tao_capture(zyla) , nMost)
    tao_send_commands, pitt, [maxPittValue(1), avgPittValue(2)]
    pause,1000
    maxPsfPos = cog(tao_capture(zyla) , nMost)

    // Calculating X axis angle relative to the camera
    axisDirection = ( maxPsfPos(1) > minPsfPos(1) ? 1 : -1 )
    angle = axisDirection * atan( (maxPsfPos(2) - minPsfPos(2)) / (maxPsfPos(1) - minPsfPos(1)) )
    write, "Calculated angle 1 : ", angle * 180/pi, "°"
    // theta = tan( DeltaY / DeltaX )

    xScale = (maxPittValue(1) - minPittValue(1)) / sqrt( (maxPsfPos(1)-minPsfPos(1))^2 + (maxPsfPos(2)-minPsfPos(2))^2 )
    xScale = xScale * axisDirection

    // Mesuring angle and scale along Y axis of tip tilt
    tao_send_commands, pitt, minPittValue
    pause, 200
    tao_send_commands, pitt, [avgPittValue(1), minPittValue(2)]
    pause,1000
    minPsfPos = cog(tao_capture(zyla) , nMost)
    tao_send_commands, pitt, [avgPittValue(1), maxPittValue(2)]
    pause,1000
    maxPsfPos = cog(tao_capture(zyla) , nMost)

    // Calculating Y axis angle relative to the camera
    axisDirection = ( maxPsfPos(2) > minPsfPos(2) ? 1 : -1 )
    angle2 = axisDirection * atan( (maxPsfPos(1) - minPsfPos(1)) / (maxPsfPos(2) - minPsfPos(2)) )
    // theta = tan( DeltaX / DeltaY )

    yScale = (maxPittValue(2) - minPittValue(2)) / sqrt( (maxPsfPos(1)-minPsfPos(1))^2 + (maxPsfPos(2)-minPsfPos(2))^2 )
    yScale = yScale * axisDirection

    angle = (angle + angle2) / 2.
    ROTM = [[cos(-angle), -sin(-angle)], [sin(-angle), cos(-angle)]]    // Rotation Matrix

    // Scale of image in commands (tip tilt unit / pixel)
    scale = [xScale, yScale]

    save, saveObj, scale, angle, ROTM
}

write, format="Averaged angle = %f°\n", angle * 180/pi
write, format="Scale = %f PittUnit/pixel\n", scale
write, "-------------------------------\n"

/*
Pour utiliser convenablement le centre de gravité ...
Retirer le fond de l'image (sinon on fait le centre de gravité du fond de l'image).
(Dans la pratique je pourrai découper un bout de l'image.)
Se centrer sur la zone calculée à l'étape précédente (sinon le centre est déviée par le fond de l'image).
*/

/************* Paramètres pour la boucle *************/
nextPosition = currentPosition = [avgPittValue(1), avgPittValue(1)]
tao_send_commands, pitt, currentPosition
pause, 1000
historySize = 5
index = 1
delta = [0.,0.]
positionHistory= array([0.,0.], historySize)
gainI = 0.8
gainE = 0.2
img = tao_capture(zyla)
if(dark==[]) dark = min(img(*))

/************* Boucle Principale *************/
for(i=1; i<=200; i++){
    image = tao_capture(zyla) - dark

    cogImage = cog(image, nMost)
    write, format="Target = %f\n", target
    write, format="Cog image = %f\n", cogImage
    delta = (target - cogImage)
    write, format="Delta = %f\n", delta

    correction = ROTM(+,) * delta(+) * scale
    write, format="Correction = %f\n", correction

    nextPosition = currentPosition + correction

    write, format="nextPosition : %f\n", nextPosition

    // Averaging nextPosition with previous ones.
    /*
    y = gi Sum( xi ) + ge xn
    y = commande
    gi = gain (probablement 1 - ge)
    xi = N commandes précédentes
    ge = gain (+ fort si peu de bruit) < 1
    xn = commande calculée pour le cas actuel
    Voir ...
    https://fr.wikipedia.org/wiki/R%C3%A9gulateur_PID
    https://fr.wikipedia.org/wiki/Filtre_%C3%A0_r%C3%A9ponse_impulsionnelle_infinie
    */
    nextPosition(1) = gainI * avg(positionHistory(1,)) + gainE * nextPosition(1,)
    nextPosition(2) = gainI * avg(positionHistory(2,)) + gainE * nextPosition(2,)

    write, format="nextPosition after averaging : %f\n", nextPosition
    nextPosition = min(nextPosition, maxPittValue)
    nextPosition = max(nextPosition, minPittValue)
    write, format="nextPosition bornée : %f\n", nextPosition

    positionHistory(,index) = nextPosition
    index = (index % historySize) + 1

    tao_send_commands, pitt, nextPosition
    currentPosition = nextPosition
    write, "-------------------------------"
    pause, 10
}
